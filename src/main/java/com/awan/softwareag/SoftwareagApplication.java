package com.awan.softwareag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftwareagApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoftwareagApplication.class, args);
    }

}

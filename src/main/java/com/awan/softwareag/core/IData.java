package com.awan.softwareag.core;

import lombok.NonNull;

import java.util.HashMap;

public class IData {

    @NonNull
    private final IDataCursor cursor = new IDataCursor();

    public IData() {
    }

    /**
     * Constructor digunakan sabgai simulasi data inputan
     */
    public IData(IDataInitializer iDataInitializer) {
        iDataInitializer.init(cursor);
    }

    public IDataCursor getCursor() {
        return cursor;
    }

    @FunctionalInterface
    public interface IDataInitializer {
        void init(IDataCursor map);
    }
}

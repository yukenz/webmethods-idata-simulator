package com.awan.softwareag.core;

import java.util.HashMap;

/**
 * Sebagai Wrapper untuk Map String, Object
 *
 * @author yukenz
 */
public class IDataCursor extends HashMap<String, Object> {


    /**
     * Dummy function untuk simulasi destroy
     */
    public void destroy() {
    }

    public void put(String key, IData iData) {
        IDataCursor cursor = iData.getCursor();
        put(key, cursor);
    }


}

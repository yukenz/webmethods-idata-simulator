package com.awan.softwareag.core;

public class IDataUtil {

    /**
     * Mendapatkan String pada Map Cursor
     *
     * @return null jika data bukan String
     */
    public static String getString(IDataCursor cursor, String key) {
        Object o = cursor.get(key);

        /* O harus string*/
        if (o instanceof String) {
            return (String) o;
        }
        return null;
    }

    /**
     * Mendapatkan Array of String ( String [] ) pada Map Cursor
     *
     * @return null jika data bukan Array of String ( String [] )
     */
    public static String[] getStringArray(IDataCursor cursor, String key) {
        Object o = cursor.get(key);

        /* O harus string*/
        if (o instanceof String[]) {
            return (String[]) o;
        }
        return null;
    }

    /**
     * Mendapatkan Object pada Map Cursor
     *
     * @return null jika data tidak ada
     */
    public static Object getObject(IDataCursor cursor, String key) {
        return cursor.get(key);
    }

    /**
     * Mendapatkan IData pada Map Cursor
     *
     * @return null jika data tidak ada atau bukan instance IData
     */
    public static IData getIData(IDataCursor cursor, String key) {
        Object o = cursor.get(key);

        if (o instanceof IData) {
            return (IData) o;
        }

        return null;
    }

    /**
     * Menambahkan Key String pada Map Cursor
     */
    public static void put(IDataCursor cursor, String key, String value) {
        cursor.put(key, value);
    }

    /**
     * Menambahkan Key-IData pada Map Cursor
     */
    public static void put(IDataCursor cursor, String key, IData value) {

        cursor.put(key, value.getCursor());

    }

    /**
     * Menambahkan Key-Object pada Map Cursor
     */
    public static void put(IDataCursor cursor, String key, Object value) {
        cursor.put(key, value);
    }


}

package com.awan.softwareag.core;

/**
 * Sebagai creation untuk object IData yang baru
 *
 * @author yukenz
 */
public class IDataFactory {

    /**
     * Sebagai simulasi pembuatan IData <br>
     * Sama seperti new IData()
     */
    public static IData create() {
        return new IData();
    }

}

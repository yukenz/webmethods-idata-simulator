package com.awan.softwareag;

import com.awan.softwareag.core.IData;
import com.awan.softwareag.core.IDataCursor;
import com.awan.softwareag.core.IDataUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;

public class IDataTest {

    ObjectWriter objectWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();

    @Test
    void idataTest() {


        IData credential = new IData(map -> {
            map.put("username", "string");
            map.put("password", "string");
        });

        IData pipeline = new IData(map -> {
            map.put("str", "string");
            map.put("credential", credential);
        });

        IDataCursor pipelineC = pipeline.getCursor();

        IData credentialIData = IDataUtil.getIData(pipelineC, "credential");

        try {
            String s = objectWriter.writeValueAsString(credentialIData.getCursor());
            System.out.println(s);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}

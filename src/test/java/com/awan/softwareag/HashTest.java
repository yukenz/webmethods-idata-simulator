package com.awan.softwareag;

import com.awan.softwareag.core.IData;
import com.awan.softwareag.core.IDataCursor;
import com.awan.softwareag.core.IDataFactory;
import com.awan.softwareag.core.IDataUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class HashTest {

    ObjectWriter objectWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();


    @Test
    void hashTest() {

        IData pipeline = new IData(map -> {
            map.put("message", "hello");
            map.put("type", "SHA512");
        });


        /* Init Result Pipeline */
        String resultCode, resultMessage, data = null;


        /* Read Input */
        IDataCursor cursorInput = pipeline.getCursor();
        String message = IDataUtil.getString(cursorInput, "message");
        String type = IDataUtil.getString(cursorInput, "type");
        cursorInput.destroy();

        /* IMPL */
        try {

            /* Validation */
            if (message == null || type == null) {
                throw new RuntimeException("Mandatory field can't be empty");
            }

            MessageDigest sha512 = MessageDigest.getInstance(type);
            byte[] digest = sha512.digest(message.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();

            /* Transform */
            for (byte b : digest) {
                String hex = Integer.toHexString(0xFF & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            /* Success */
            resultCode = "00";
            resultMessage = "Success";
            data = hexString.toString().toUpperCase();
        } catch (Exception e) {
            resultCode = "01";
            resultMessage = e.getMessage();
            data = null;
        }

        /* Process Result Doc */
        IData result = IDataFactory.create();
        IDataCursor cursorResult = result.getCursor();
        IDataUtil.put(cursorResult, "resultCode", resultCode);
        IDataUtil.put(cursorResult, "resultMessage", resultMessage);
        IDataUtil.put(cursorResult, "data", data);
        cursorResult.destroy();

        /* Finalize Pipeline */
        IDataCursor cursorOutput = pipeline.getCursor();
        IDataUtil.put(cursorOutput, "result", result);
        cursorOutput.destroy();

        /* SOUT */
        try {
            String s = objectWriter.writeValueAsString(cursorOutput);
            System.out.println(s);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}

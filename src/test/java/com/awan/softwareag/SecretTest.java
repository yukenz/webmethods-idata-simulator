package com.awan.softwareag;

import com.awan.softwareag.core.IData;
import com.awan.softwareag.core.IDataCursor;
import com.awan.softwareag.core.IDataFactory;
import com.awan.softwareag.core.IDataUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;

import java.util.Objects;

public class SecretTest {

    ObjectWriter objectWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();


    @Test
    void secretTest() throws JsonProcessingException {


        IData pipeline = new IData(map -> {
            map.put("apiSecret", "apiSecret");
            map.put("accessTime", "accessTime");
            map.put("apiKey", "apiKey");
            map.put("applicationId", "applicationId");
        });

        /* Init Result Pipeline */
        String resultCode, resultMessage, data;

        /* Read Input */
        IDataCursor cursorInput = pipeline.getCursor();
        String apiSecret = IDataUtil.getString(cursorInput, "apiSecret");
        String accessTime = IDataUtil.getString(cursorInput, "accessTime");
        String apiKey = IDataUtil.getString(cursorInput, "apiKey");
        String applicationId = IDataUtil.getString(cursorInput, "applicationId");
        cursorInput.destroy();

        /* IMPL */
        try {

            if (Objects.isNull(apiSecret)
                    || Objects.isNull(accessTime)
                    || Objects.isNull(apiKey)
                    || Objects.isNull(applicationId)
            ) {
                throw new RuntimeException("Mandatory Field can't be empty");
            }

            String message = String.format("apiSecret=%s&accessTime=%s&apiKey=%s&applicationId=%s",
                    apiSecret, accessTime, apiKey, applicationId);

            /* Success */
            resultCode = "00";
            resultMessage = "Success";
            data = message;
        } catch (Exception e) {
            resultCode = "01";
            resultMessage = e.getMessage();
            data = null;
        }

        /* Process Result Doc */
        IData result = IDataFactory.create();
        IDataCursor cursorResult = result.getCursor();
        IDataUtil.put(cursorResult, "resultCode", resultCode);
        IDataUtil.put(cursorResult, "resultMessage", resultMessage);
        IDataUtil.put(cursorResult, "data", data);
        cursorResult.destroy();

        /* Finalize Pipeline */
        IDataCursor cursorOutput = pipeline.getCursor();
        IDataUtil.put(cursorOutput, "result", result);
        cursorOutput.destroy();

        System.out.println(objectWriter.writeValueAsString(cursorOutput));

    }
}

package com.awan.softwareag;

import com.awan.softwareag.core.IData;
import com.awan.softwareag.core.IDataCursor;
import com.awan.softwareag.core.IDataFactory;
import com.awan.softwareag.core.IDataUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

public class NewIDataTest {

    ObjectWriter objectWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();


    @Test
    void testNewIData() {

        IData pipeline = new IData(map -> {
            map.put("satu", "def");
            map.put("dua", "def");
        });

        IDataCursor cursor = pipeline.getCursor();

        System.out.println(cursor instanceof HashMap);
        System.out.println(cursor);

    }

    @Test
    void testNested() throws JsonProcessingException {


        IData bInit = IDataFactory.create();
        IDataCursor cursorbInit = bInit.getCursor();
        IDataUtil.put(cursorbInit, "c", "cValue");


        IData requestInit = IDataFactory.create();
        IDataCursor cursorRequestInit = requestInit.getCursor();
        IDataUtil.put(cursorRequestInit, "b", bInit);

        IData pipeline = new IData(map -> {
            map.put("request", requestInit);
        });


// pipeline
        IDataCursor pipelineCursor = pipeline.getCursor();

        // request
        IData request = IDataUtil.getIData(pipelineCursor, "request");
        if (request != null) {
            IDataCursor requestCursor = request.getCursor();

            // i.b
            IData b = IDataUtil.getIData(requestCursor, "b");
            if (b != null) {
                IDataCursor bCursor = b.getCursor();
                String c = IDataUtil.getString(bCursor, "c");
                bCursor.destroy();
            }
            requestCursor.destroy();
        }
        pipelineCursor.destroy();

// pipeline
        IDataCursor pipelineCursor_1 = pipeline.getCursor();

// response
        IData response = IDataFactory.create();
        IDataCursor responseCursor = response.getCursor();

// response.c
        IData c_1 = IDataFactory.create();

        IDataUtil.put(responseCursor, "c", bInit);
        responseCursor.destroy();
        IDataUtil.put(pipelineCursor_1, "response", response);
        pipelineCursor_1.destroy();

        System.out.println(objectWriter.writeValueAsString(pipelineCursor_1));


    }
}

package com.awan.softwareag;

import com.awan.softwareag.core.IData;
import com.awan.softwareag.core.IDataCursor;
import com.awan.softwareag.core.IDataFactory;
import com.awan.softwareag.core.IDataUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import java.util.stream.IntStream;

public class TrueValueTest {

    @Test
    void mapTruth() {

        IntStream intStream = IntStream.of(1, 2, 3, 4);

        System.out.println((intStream.allMatch(value -> {

            if (value == 3) {
                return false;
            }

            return true;
        })));

        intStream = IntStream.of(1, 2, 3, 4);

        System.out.println((intStream.anyMatch(value -> {

            if (value == 3) {
                return false;
            }

            return true;
        })));


    }

    @Test
    void testAnyMatch() {


        IData pipeline = new IData(map -> {
            map.put("predicate", new String[]{"true", "false"});
        });

        Boolean result;

// pipeline
        IDataCursor pipelineCursor = pipeline.getCursor();

        String[] predicate = IDataUtil.getStringArray(pipelineCursor, "predicate");
        pipelineCursor.destroy();

        try {
            result = Arrays.stream(predicate)
                    .anyMatch(Boolean::valueOf);
        } catch (Exception e) {
            result = false;
        }

// pipeline
        IDataCursor pipelineCursor_1 = pipeline.getCursor();
        IDataUtil.put(pipelineCursor_1, "result", result.toString());
        pipelineCursor_1.destroy();

//        System.out.println(pipeline.getCursor());

    }

    @Test
    void testEqualMatcher() {


        IData pipeline = new IData(map -> {

            map.put("expected", new String[]{"awan", "good"});
            map.put("actual", new String[]{"awan", "gooed"});

        });

// pipeline
        IDataCursor pipelineCursor = pipeline.getCursor();
        String[] listExpected = IDataUtil.getStringArray(pipelineCursor, "expected");
        String[] listActual = IDataUtil.getStringArray(pipelineCursor, "actual");
        pipelineCursor.destroy();

        String[] result;
        String errorMessage;

// Process

        try {

            if (listExpected.length != listActual.length) {
                throw new IllegalArgumentException("expected and actual length not same");
            }

            LinkedList<String> resultMatchList = new LinkedList<>();
            for (int i = 0; i < listExpected.length; i++) {
                boolean resultMatcher = listExpected[i].equals(listActual[i]);
                resultMatchList.addLast(Boolean.toString(resultMatcher));
            }

            result = resultMatchList.toArray(String[]::new);
            errorMessage = "success";

        } catch (Exception e) {
            result = null;
            errorMessage = e.getMessage();
        }

// result
        IData resultIData = IDataFactory.create();
        IDataCursor resultCursor = resultIData.getCursor();

        IDataUtil.put(resultCursor, "errorMessage", errorMessage);
        IDataUtil.put(resultCursor, "resultMatcher", result);
        resultCursor.destroy();

        IDataCursor pipelineCursor_1 = pipeline.getCursor();
        IDataUtil.put(pipelineCursor_1, "result", resultIData);
        pipelineCursor_1.destroy();

        for (int i = 0; i < listExpected.length; i++) {
            System.out.println("Expected: " + listExpected[i]);
            System.out.println("Actual: " + listActual[i]);
            System.out.println("Result: " + result[i]);
        }


    }
}
